@echo off
:: file : 20220128`1811 (after 20220123`0821)
:: summary : Compact the Git repository in two steps
::    (1) Expire unreachable content and pack repo most compact
::    (2) Clear the reflog as well
:: reference : https://stackoverflow.com/questions/35410336/how-to-compact-local-git-repo [ref 20220123`0812]
:: reference : https://stackoverflow.com/questions/11004045/batch-file-counting-number-of-files-in-folder-and-storing-in-a-variable [ref 20220128`1813]

color 2f
title This is %~n0%~x0 by user %USERNAME%
echo ***************************************************************
echo *** thisdrive = %~d0
echo *** thisdir   = %~dp0
echo *** CWD       = %cd%
echo *** option    = %1
echo ***************************************************************

:: Prepare CWD
%~d0
cd %~dp0
cd ..
echo *** CWD = %cd%

:: Count files
:: Todo -- Count in subdirectories as well!
cd .git
set count=0
setlocal enableextensions
FOR %%x IN (*) DO SET /A count+=1
echo *** Number of files in .git = %count%
endlocal
cd ..

:: User dialog
echo *** Compact this Git repo now?
pause

:: Execute the actual task
@echo on
git gc --aggressive --prune=now
git reflog expire --expire=now --all
@echo off

:: Count files
:: Todo -- Count in subdirectories as well!
cd .git
set count=0
setlocal enableextensions
FOR %%x IN (*) DO SET /A count+=1
echo *** Number of files in .git = %count%
endlocal
cd ..

pause
