﻿# TickediTack Issues

### Owl Clock Icon Outline

The lines of the Owl Clock Icon are thinned out by the dowsizing from
 a large image.
 The lines have to be thickened
 &nbsp; <sup><sub>*[Issue 20210904°1311 Icon line thickness]*</sub></sup>

<sup><sub><sup>*[File 20210904°0915]* ܀Ω</sup></sub></sup>
