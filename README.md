<img src="./docs/20210904o1113.owl-clock-2.v2.x0256y0256.png" align="right" width="180" height="180" alt="Owl Clock Icon">

# TickediTack &nbsp; <sup><sub><sup>v0.0.2..</sup></sub></sup>

### Synopsis

Slogan : Time telling with PHP and JavaScript

Summary : This demonstrates a ticking clock on a HTML page in multiple PHP and JavaScript techniques.

Demonstrated techiques :

1. Use **plain PHP**. The clock shows time of the request. It cannot
 tick by itself, it only shows the time of page load. This shows the
 time from the server.

2. Ignit **periodic requests** by JavaScript. This makes above
 plain PHP tick. This is still just the time from page request,
 but now the request happens periodically.

3. Use **pure JavaScript**. The clock ticks with no PHP involved. This shows
 the time from the local machine.

4. Use **AJAX to PHP**. The time is actualized from a PHP script, which is
 called by a JavaScript/AJAX request — as opposed to the page request in
 above plain PHP technique.

5. Use **external time service**. Here we contact
 [worldtimeapi.org](https://worldtimeapi.org)
 to serve the time. This service may be slow, so give it time to answer.

The project by the way also demonstrates :

- How to use the JavaScript **timer**

- How to facilitate **AJAX** (the XMLHttpRequest object)

- How to read and write a **cookie**

- The JavaScript **`forEach`** loop

License : BSD 3-Clause License

Copyright : Norbert C. Maier

Project status : Quick'n'dirty. Only one all-in-one-page so far.

### Todo

- Add a dedicated minimalistic page for each technique,
 so the single techniques are seen more clearly.

- Add visit counter to PHP to show the difference between a page request
 and an AJAX request. The one will increase the counter, the other will not.

- Utilize other time services, e.g. [worldclockapi.com](http://worldclockapi.com/)

### References on Time Telling

Here are some references around time telling :

<img src="./docs/icos/20240703o1913.time-is.v2.x0048y0048.png" align="left" width="48" height="48" alt="Link icon for uhr.ptb.de">
 &nbsp;
 The time service
 <a href="https://time.is/" style="font-weight:bold;">time.is</a>
 is providing high precision time for all time zones.
 It offers a widget for embedding the clock on your own site, see
 <a href="https://time.is/widgets">time.is/widgets</a>.

<img src="./docs/imgs/20220128o1815.uhr-ptb-de.v1.x0048y0048.png" align="left" width="48" height="48" alt="Link icon for uhr.ptb.de">
 &nbsp;
 An external time server is
 <a href="https://uhr.ptb.de/" style="font-weight:bold;">uhr.ptb.de</a>
 run by the
 <a href="https://www.ptb.de/">PTB</a>
 Braunschweig, see article
 <a href="https://www.ptb.de/cms/de/ptb/fachabteilungen/abtq/gruppe-q4/ref-q42/zeitsynchronisation-von-rechnern-mit-hilfe-des-network-time-protocol-ntp.html">Zeitverteilung mit IP</a>
 . Only that is not much help for us here, because it is using the NTP protocol, which is too complicated with JavaScript.

&nbsp;

<img src="./docs/icos/20210220o1213.timeanddate.v1.x0048y0048.png" align="left" width="48" height="48" alt="Link icon 20210220°1213">
 &nbsp;
 The <a href="https://www.timeanddate.com/" style="font-weight:bold;">Time and Date AS</a> <!--- [ref ] --->
 provides various time service APIs.
 (AS = Aksjeselskap, this is Norwegian for 'Joint stock company'.)

&nbsp;

<img src="./docs/imgs/20210914o1917.worldclockapi.v2.x0048y0048.png" align="left" width="48" height="48" alt="Link icon for worldclockapi.com">
 &nbsp;
 The <a href="http://worldclockapi.com/" style="font-weight:bold;">World Clock API</a>
 is another time service for possible use as an external time server with JavaScript.

&nbsp;

<img src="./docs/imgs/20220128o1713.onlineclock.v1.x0064y0064.png" align="left" width="60" height="60" alt="Link icon for onlineclock.net">
 &nbsp;
 <a href="https://onlineclock.net/" style="font-weight:bold;">onlineclock.net</a>
 with funny clocks like
 <a href="https://binary.onlineclock.net/" style="font-weight:bold;">binary.onlineclock.net</a>.

&nbsp;

<img src="./docs/icos/20220204o1933.inyourowntime.v2.x0064y0064.png" align="left" width="56" height="56" alt="Link icon for www.inyourowntime.zone">
 &nbsp;
 Automatic time zone converter
 <a href="https://www.inyourowntime.zone/" style="font-weight:bold;" data-reference="20220204°1932">inyourowntime.zone</a>
 — Very comfortable to announce event dates to a global audience, e.g. Santa Claus 2033:
 <a href="https://www.inyourowntime.zone/2033-12-06_11.11_Europe.Berlin" style="" data-reference="">2033-12-06\_11.11\_Europe.Berlin</a>
 . It also checks whether the browsers internal clock is in sync with it's own server time.

&nbsp;

<img src="./docs/icos/20220212o0933.gaijin.v2.x0064y0064.png" align="left" width="48" height="48" alt="Link icon for www.gaijin.at">
 &nbsp;
 Time Converter service
 <a href="https://www.gaijin.at/en/tools/time-converter" style="font-weight:bold;">www.gaijin.at/en/tools/time-converter</a>
 &nbsp; <!-- sup><sub><sup>*[ref 20220212°0952]*</sup></sub></sup -->
 converts between german Date-and-Time, Date-String, UNIX-Epoch-Time, CF-Absolute-Time and some others

&nbsp;

### References on other services

Though this links are not about time telling but
 on web services sometimes associated with time telling.

<img src="./docs/icos/20220617o1132.ipapi.v2.x0064y0064.png" align="left" width="48" height="48" alt="Link icon for IP-API">
 &nbsp;
 IP Geolocation API
 <a href="https://ip-api.com/" style="font-weight:bold;">ip-api.com</a>
 &nbsp; <!-- sup><sub><sup>*[ref 20220617°1131]*</sup></sub></sup -->
 tells the location of any given IP-Address

&nbsp;

<img src="./docs/icos/20220617o1135.openweathermap.v1.x0064y0064.png" align="left" width="48" height="48" alt="Link icon for OpenWeather">
 &nbsp;
 The OpenWeather project at
 <a href="https://openweathermap.org/">openweathermap.org</a>
 &nbsp; <!-- sup><sub><sup>*[ref 20220617°1137]*</sup></sub></sup -->
 provides a
 <a href="https://openweathermap.org/api" style="font-weight:bold;">Weather API</a>
 &nbsp; <!-- sup><sub><sup>*[ref 20220617°1134]*</sup></sub></sup -->
 , which tells a weather forcast for any town on earth

&nbsp;

<img src="./docs/icos/20110902o1742.codeproject.v3.x0064y0064.png" align="left" width="64" height="64" alt="Link icon for CodeProject">
 &nbsp;
 Codeproject article
 <a href="https://www.codeproject.com/Articles/5335096/A-Web-Enabled-Smart-Clock-and-Weather-Station" style="">A Web Enabled Smart Clock and Weather Station</a>
 &nbsp; <!-- sup><sub><sup>*[ref 20220617°1124]*</sup></sub></sup -->
 provides C++ example code to access the above two services

&nbsp;

<img src="./docs/icos/20200702o0233.heise.v1.x0048y0048.png" align="left" width="48" height="48" alt="Link icon for Heise">
 &nbsp;
 Heise article
 <a href="https://www.heise.de/news/Weltweit-erste-automatische-Zeitansage-ist-im-Ruhestand-7159634.html" style="">Weltweit erste automatische Zeitansage ist im Ruhestand</a>
 about the historic french telephonic time telling service
 &nbsp; <!-- sup><sub><sup>*[ref 20220725°1112]*</sup></sub></sup -->
 . Heise article
 <a href="https://www.heise.de/ratgeber/Zeitansage-mit-Raspberry-Pi-4403870.html" style="">Zeitansage mit dem Raspberry Pi</a>
 on building a voice clock with Raspberry Pi (the full article is subject to charge)
 &nbsp; <!-- sup><sub><sup>*[ref 20220725°1113]*</sup></sub></sup -->

&nbsp;

<img src="./docs/icos/20220725o1115.telekom.v2.x0048y0048.png" align="left" width="48" height="48" alt="Link icon for Telekom">
 &nbsp;
 Telekom page
 <a href="https://servicenummern.telekom.de/weitere-informationen/zeitansage/" style="">Beim nächsten Ton ist es …</a>
 tells details about the telephonic time telling service in Germany
 &nbsp; <!-- sup><sub><sup>*[ref 20220725°1114]*</sup></sub></sup -->
 
&nbsp;

### Credits

The Owl Clock logo on top right comes from
[openclipart.org/detail/247152/owl-clock](https://openclipart.org/detail/247152/owl-clock).
 Many thanks to the unknow original author and those who refined and uploaded it.

### Finally

Find my other projects on
 [github.com/normai/](https://github.com/normai/)

Bye,<br>Norbert<br>2022-Dec-01 &nbsp; *<del>2022-Jan-28</del> &nbsp; <del>2021-Sep-15</del>*

<sup><sub><sup>*[page 20210904°0912, project 20210904°0911]* ܀Ω</sup></sub></sup>
